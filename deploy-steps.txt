echo "This is the deploy step"

export PROJECT_ID=cloud-assignment3-233903
gcloud config set project $PROJECT_ID
export CLOUDSDK_COMPUTE_ZONE=us-central1-b

cd /home/ubuntu/.jenkins/workspace/movieAppCICD

gcloud container clusters get-credentials moviecluster4

kubectl delete deployment webapp-deployment4 || echo "webapp-deployment4 deployment does not exist"
kubectl delete service webapp-deployment4 || echo "webapp-deployment4 service does not exist"
kubectl delete ingress webapp-ingress || echo "webapp-ingress does not exist"

kubectl create -f deployment.yaml
kubectl expose deployment webapp-deployment4 --target-port=5000 --type=NodePort

kubectl apply -f ingress.yaml

echo "Done deploying"
