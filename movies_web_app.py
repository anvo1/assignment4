import os
import time
import datetime

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__, template_folder='templates')
app = application

'''
CREATE TABLE movies (
     ID MEDIUMINT NOT NULL AUTO_INCREMENT,
     Year YEAR(4) NOT NULL,
     Title CHAR(100) NOT NULL,
     Director CHAR(100) NOT NULL,
     Actor CHAR(100) NOT NULL,
     Release_Date DATE NOT NULL,
     Rating INTEGER NOT NULL,
     PRIMARY KEY (ID)
);
'''

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def query_movies(title):

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT * FROM movies WHERE LOWER(title)='" + title + "';")
    movies = [dict(id=row[0], year=row[1], title=row[2],director=row[3]) for row in cur.fetchall()]
    return movies


@app.route('/add_movie', methods=['POST'])
def add_movie_db():
    print("Received request.")
    message = ''
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    movies_with_title = query_movies(title)
    if(len(movies_with_title) > 0):
        message = "Movie " + title + " could not be inserted - movies with title existed"
        return hello(message=message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    data = year + ",'" + title + "','" + director + "','" + actor + "', STR_TO_DATE('" + release_date + "', '%d %M %Y')" + "," + rating
    query = "INSERT INTO movies (Year,Title,Director,Actor,Release_Date,Rating) values (" + data + ")"
    try:
        cur.execute(query)
        cnx.commit()
        message = "Movie " + title + " successfully inserted"
    except Exception as exp:
        message = "Movie " + title + " could not be inserted - " + str(exp)

    return hello(message=message)

@app.route('/delete_movie', methods=['POST'])
def delete_movie_db():
    print("Received request.")
    message = ''
    title = request.form['delete_title']
    movies_with_title = query_movies(title.lower())
    if(len(movies_with_title) == 0):
        message = "Movie with " + title + " does not exist"
        return hello(message=message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    query = "DELETE FROM movies WHERE Title='" + title + "';"
    print(query)
    try:
        cur.execute(query)
        cnx.commit()
        message = "Movie " + title + " successfully deleted"
    except Exception as exp:
        message = "Movie " + title + " could not be deleted - " + str(exp)

    return hello(message=message)

@app.route('/update_movie', methods=['POST'])
def update_movie_db():
    print("Received request.")
    message = ''
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    data = "Year=" + year + ", Director='" + director + "', Actor='" + actor + "', Release_Date=STR_TO_DATE('" + release_date + "', '%d %M %Y'), Rating=" + rating
    query = "UPDATE movies SET " + data + " WHERE Title='" + title + "';"
    print(query)
    try:
        cur.execute(query)
        cnx.commit()
        message = "Movie " + title + " successfully updated"
    except Exception as exp:
        message = "Movie " + title + " could not be inserted - " + str(exp)

    return hello(message=message)

@app.route('/search_movie', methods=['GET'])
def search_movie_db():
    print("Received request.")
    message = ''
    actor = request.args.get("search_actor")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    query = "SELECT * FROM movies WHERE LOWER(Actor)='" + actor.lower() + "';"
    print(query)
    try:
        cur.execute(query)
        movies = [dict(title=row[2], year=row[1], actor=row[4]) for row in cur.fetchall()]
        if len(movies) == 0:
            message = "No movies found for actor " + actor
        else:
            for movie in movies:
                message += movie['title'] + ", " + str(movie['year']) + ", " + movie['actor'] + " \n"
    except Exception as exp:
        message = "Could not search movie with actor " +   actor  +" - " + str(exp)

    return hello(message=message)


def query_one_movie(highest_rating):
    db, username, password, hostname = get_db_creds()
    message = ""
    order = "MIN"
    if highest_rating:
        order = "MAX"

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    query = "SELECT * FROM movies WHERE Rating = (SELECT " + order +  "(Rating) FROM movies)"
    print(query)
    try:
        cur.execute(query)
        movies = [dict(title=row[2], year=row[1], actor=row[4], director=row[3], rating=row[6]) for row in cur.fetchall()]
        for movie in movies:
            message += movie['title'] + ", " + str(movie['year']) + ", " + movie['actor'] + ", " + movie['director'] + ", " + str(movie['rating']) + " \n"
    except Exception as exp:
        message = "Could not list movies - " + str(exp)

    return message

@app.route('/highest_rating', methods=['GET'])
def highest_rating_movie_db():
    print("Received request.")
    message = query_one_movie(True)
    return hello(message=message)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating_movie_db():
    print("Received request.")
    message = query_one_movie(False)
    return hello(message=message)

@app.route("/")
def hello(message=''):
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html', message=message.replace("\n", "<br />"))

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
